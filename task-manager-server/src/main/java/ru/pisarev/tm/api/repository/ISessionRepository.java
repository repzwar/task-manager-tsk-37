package ru.pisarev.tm.api.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.Nullable;
import ru.pisarev.tm.api.IRepository;
import ru.pisarev.tm.model.Session;

import java.util.List;

public interface ISessionRepository extends IRepository<Session> {
    @Nullable List<Session> findAllByUserId(@Nullable String userId);

    void removeByUserId(@Nullable String userId);

    @Nullable
    @SneakyThrows
    Session add(@Nullable Session entity);

    @Nullable
    @SneakyThrows
    Session update(@Nullable Session entity);
}
