package ru.pisarev.tm.exception.entity;

import org.jetbrains.annotations.NotNull;
import ru.pisarev.tm.exception.AbstractException;

public class LoginExistException extends AbstractException {

    @NotNull
    public LoginExistException() {
        super("Error. Login already exist.");
    }

    @NotNull
    public LoginExistException(String value) {
        super("Error. Login '" + value + "' already exist.");
    }

}
