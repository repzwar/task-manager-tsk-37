package ru.pisarev.tm.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pisarev.tm.model.AbstractBusinessEntity;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public abstract class AbstractBusinessRepository<E extends AbstractBusinessEntity> extends AbstractRepository<E> {

    public AbstractBusinessRepository(@NotNull final Connection connection) {
        super(connection);
    }

    public abstract E add(@NotNull final String userId, @Nullable final E entity);

    @NotNull
    @SneakyThrows
    public List<E> findAll(@NotNull final String userId) {
        @NotNull final String query = "SELECT * FROM " + getTableName() + " where user_id = ?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, userId);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        @NotNull final List<E> result = new ArrayList<>();
        while (resultSet.next()) result.add(fetch(resultSet));
        statement.close();
        return result;
    }

    @Nullable
    @SneakyThrows
    public E findById(@NotNull final String userId, @Nullable final String id) {
        @NotNull final String query = "SELECT * FROM " + getTableName() + " where id = ? and user_id=? limit 1";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, id);
        statement.setString(2, userId);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        final boolean hasNext = resultSet.next();
        if (!hasNext) return null;
        @NotNull final E result = fetch(resultSet);
        statement.close();
        return result;
    }

    public void addAll(@NotNull final String userId, @Nullable final Collection<E> collection) {
        if (collection == null) return;
        for (E item : collection) {
            item.setUserId(userId);
            add(item);
        }
    }

    @SneakyThrows
    public void clear(@NotNull final String userId) {
        @NotNull final String query = "delete from " + getTableName() + " where user_id = ?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, userId);
        statement.executeUpdate();
        statement.close();
    }

    @SneakyThrows
    public void removeById(@NotNull final String userId, @Nullable final String id) {
        @NotNull final String query = "delete from " + getTableName() + " where id = ? and user_id = ?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, id);
        statement.setString(2, userId);
        statement.executeUpdate();
        statement.close();
    }

    @SneakyThrows
    public void remove(@NotNull final String userId, @NotNull final E entity) {
        @NotNull final String query = "delete from " + getTableName() + " where id = ? and user_id = ?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, entity.getId());
        statement.setString(2, userId);
        statement.executeUpdate();
        statement.close();
    }

}
